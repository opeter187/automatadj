package com.opeter.automatadj;

import com.opeter.automatadj.test.DinamicSorterTest;
import com.opeter.automatadj.test.RandomSorterTest;
import com.opeter.automatadj.test.StaticSorterTest;
import com.opeter.automatadj.test.VotedPlaylistTest;


public class Main {
    public static void main(String[] args) {

        System.out.println("Static sorter:");
        new StaticSorterTest().staticTest();

        System.out.println("\n\n\nDinamic sorter: Somebody comes to the party");
        new DinamicSorterTest().somebodyComes();

        System.out.println("\n\n\nDinamic sorter: Somebody leaves the party");
        new DinamicSorterTest().somebodyLeaveTheParty();

        System.out.println("\n\n\nVotedPlaylist: remove duplicated \"Sultans of swing\" and left 4 \"Black Dog\"");
        new VotedPlaylistTest().removerRedundantTracksTest();

        System.out.println("\n\n\nRandom sorter:");
        new RandomSorterTest().randomTest();
    }


}
