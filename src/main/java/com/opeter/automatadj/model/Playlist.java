package com.opeter.automatadj.model;

import java.util.ArrayList;
import java.util.List;

public class Playlist {
    private final List<TrackInterface> tracks = new ArrayList<>();

    public Playlist() {
    }

    public Playlist(List<TrackInterface> list) {
        tracks.addAll(list);
    }

    public void addTrack(Track t) {
        tracks.add(t);
    }

    public List<TrackInterface> getTrackList() {
        return tracks;
    }


}
