package com.opeter.automatadj.model;

public record Track(String artist, String track) implements TrackInterface {
}

