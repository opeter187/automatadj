package com.opeter.automatadj.model;

public interface TrackInterface {
    String artist();

    String track();
}
