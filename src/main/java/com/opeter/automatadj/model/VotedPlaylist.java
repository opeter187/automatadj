package com.opeter.automatadj.model;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.stream.Collectors;

public class VotedPlaylist implements VotedPlaylistInterface {
    private final Map<TrackInterface, Integer> map = new HashMap<>();

    @Override
    public void addPlaylist(Playlist playlist) {
        for (TrackInterface track : playlist.getTrackList()) {
            Integer originalValue = map.getOrDefault(track, 0);
            map.put(track, originalValue + 1);
        }
    }

    @Override
    public void addPlaylist(Playlist playlist, boolean removeRedundantTracks) {
        if (removeRedundantTracks) {
            HashSet<TrackInterface> set = new HashSet<>(playlist.getTrackList());
            addPlaylist(new Playlist(set.stream().toList()));
        } else {
            addPlaylist(playlist);
        }
    }


    public Map<TrackInterface, Integer> getVotedPlaylist() {
        return map;
    }

    public void remove(TrackInterface track) {
        map.remove(track);
    }

    public int getVotes(TrackInterface track) {
        return map.getOrDefault(track, 0);
    }

    @Override
    public String toString() {
        return map.entrySet().stream().map(e ->
                e.getKey().artist() + " - "
                        + e.getKey().track() + " = "
                        + e.getValue() + "\n").collect(Collectors.joining());
    }
}
