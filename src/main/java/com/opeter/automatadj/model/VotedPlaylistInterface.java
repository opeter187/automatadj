package com.opeter.automatadj.model;

import java.util.Map;

public interface VotedPlaylistInterface {

    void addPlaylist(Playlist playlist);

    void addPlaylist(Playlist playlist, boolean removeRedundantTracks);

    Map<TrackInterface, Integer> getVotedPlaylist();

    int getVotes(TrackInterface track);

    void remove(TrackInterface track);
}
