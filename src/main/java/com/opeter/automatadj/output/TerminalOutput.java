package com.opeter.automatadj.output;

import com.opeter.automatadj.model.TrackInterface;

import java.util.List;

public class TerminalOutput {
    public void sendPlaylist(List<TrackInterface> playlist) {
        for (TrackInterface track : playlist) {
            printTrack(track);
        }
    }

    public void playTrack(TrackInterface track) {
        if (track == null) {
            noMoreTracks();
        } else printTrack(track);
    }

    private void noMoreTracks() {
        print("End of the Playlist.");
    }

    private void printTrack(TrackInterface track) {
        print(track.artist() + " - " + track.track());
    }

    public void print(String s) {
        System.out.println(s);
    }
}
