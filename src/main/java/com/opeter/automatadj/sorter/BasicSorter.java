package com.opeter.automatadj.sorter;

import com.opeter.automatadj.model.TrackInterface;
import com.opeter.automatadj.model.VotedPlaylistInterface;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

public class BasicSorter implements TrackSorterInterface {

    @Override
    public List<TrackInterface> sort(VotedPlaylistInterface votedPlaylist) {
        List<Map.Entry<TrackInterface, Integer>> entries = new ArrayList<>(votedPlaylist.getVotedPlaylist().entrySet());
        entries.sort(Map.Entry.comparingByValue());
        Collections.reverse(entries);

        return entries.stream().map(Map.Entry::getKey).toList();
    }
}
