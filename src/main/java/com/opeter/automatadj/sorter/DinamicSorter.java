package com.opeter.automatadj.sorter;

import com.opeter.automatadj.model.Playlist;
import com.opeter.automatadj.model.TrackInterface;
import com.opeter.automatadj.model.VotedPlaylistInterface;

import java.util.List;

public class DinamicSorter extends BasicSorter {
    private final VotedPlaylistInterface votedPlaylist;

    public DinamicSorter(VotedPlaylistInterface votedPlaylist) {
        this.votedPlaylist = votedPlaylist;
    }

    public TrackInterface getNextTrack() {
        if (votedPlaylist.getVotedPlaylist().isEmpty()) return null;
        List<TrackInterface> sortPlaylist = super.sort(votedPlaylist);
        TrackInterface nextTrack = sortPlaylist.get(0);
        votedPlaylist.remove(nextTrack);
        return nextTrack;
    }

    public void addPlaylist(Playlist playlist) {
        votedPlaylist.addPlaylist(playlist);
    }

    public void removePlaylist(Playlist playlist) {
        for (TrackInterface track : playlist.getTrackList()) {
            int votes = votedPlaylist.getVotes(track);
            if (votes == 1) {
                votedPlaylist.remove(track);
            }
            if (1 < votes) {
                votedPlaylist.getVotedPlaylist().put(track, votes - 1);
            }
        }
    }
}
