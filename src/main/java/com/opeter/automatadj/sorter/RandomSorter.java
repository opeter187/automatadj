package com.opeter.automatadj.sorter;

import com.opeter.automatadj.model.TrackInterface;
import com.opeter.automatadj.model.VotedPlaylistInterface;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class RandomSorter implements TrackSorterInterface {

    @Override
    public List<TrackInterface> sort(VotedPlaylistInterface votedPlaylist) {
        List<TrackInterface> playlist = new ArrayList<>(votedPlaylist.getVotedPlaylist().keySet());
        Collections.shuffle(playlist);
        return playlist;
    }
}
