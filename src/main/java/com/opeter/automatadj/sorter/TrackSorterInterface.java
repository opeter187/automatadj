package com.opeter.automatadj.sorter;

import com.opeter.automatadj.model.TrackInterface;
import com.opeter.automatadj.model.VotedPlaylistInterface;

import java.util.List;

public interface TrackSorterInterface {
    List<TrackInterface> sort(VotedPlaylistInterface votedPlaylist);
}
