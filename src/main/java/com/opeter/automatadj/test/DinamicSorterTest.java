package com.opeter.automatadj.test;

import com.opeter.automatadj.model.VotedPlaylist;
import com.opeter.automatadj.model.VotedPlaylistInterface;
import com.opeter.automatadj.output.TerminalOutput;
import com.opeter.automatadj.sorter.DinamicSorter;

import java.util.logging.Level;
import java.util.logging.Logger;

public class DinamicSorterTest {
    private final Logger logger = Logger.getLogger(DinamicSorterTest.class.getName());

    public void somebodyComes() {
        VotedPlaylistInterface votedPlaylist = new VotedPlaylist();
        TerminalOutput terminalOutput = new TerminalOutput();
        DinamicSorter dinamicSorter = new DinamicSorter(votedPlaylist);

        TestInput testInput = new TestInput();
        dinamicSorter.addPlaylist(testInput.getPL1());
        dinamicSorter.addPlaylist(testInput.getPL2());
        log(votedPlaylist);

        terminalOutput.playTrack(dinamicSorter.getNextTrack());
        log(votedPlaylist);

        terminalOutput.playTrack(dinamicSorter.getNextTrack());
        log(votedPlaylist);

        terminalOutput.playTrack(dinamicSorter.getNextTrack());
        log(votedPlaylist);

        terminalOutput.print("\nSomebody is coming to the party with a new playlist.\n");
        dinamicSorter.addPlaylist(testInput.getPL3());
        log(votedPlaylist);

        terminalOutput.playTrack(dinamicSorter.getNextTrack());
        log(votedPlaylist);

        terminalOutput.playTrack(dinamicSorter.getNextTrack());
        log(votedPlaylist);

        terminalOutput.playTrack(dinamicSorter.getNextTrack());
    }

    public void somebodyLeaveTheParty() {
        VotedPlaylistInterface votedPlaylist = new VotedPlaylist();
        TerminalOutput terminalOutput = new TerminalOutput();
        DinamicSorter dinamicSorter = new DinamicSorter(votedPlaylist);

        TestInput testInput = new TestInput();
        dinamicSorter.addPlaylist(testInput.getPL1());
        dinamicSorter.addPlaylist(testInput.getPL3());
        log(votedPlaylist);

        terminalOutput.playTrack(dinamicSorter.getNextTrack());
        log(votedPlaylist);

        terminalOutput.print("\nSomebody is leaving to the party and remove her playlist.\n");
        dinamicSorter.removePlaylist(testInput.getPL3());
        log(votedPlaylist);

        terminalOutput.playTrack(dinamicSorter.getNextTrack());
        log(votedPlaylist);

    }

    private void log(VotedPlaylistInterface votedPlaylist) {
        logger.log(Level.INFO, votedPlaylist::toString);
    }
}
