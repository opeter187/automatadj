package com.opeter.automatadj.test;

import com.opeter.automatadj.model.TrackInterface;
import com.opeter.automatadj.model.VotedPlaylist;
import com.opeter.automatadj.model.VotedPlaylistInterface;
import com.opeter.automatadj.output.TerminalOutput;
import com.opeter.automatadj.sorter.RandomSorter;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public class RandomSorterTest {

    public void randomTest() {
        Logger logger = Logger.getLogger(RandomSorterTest.class.getName());

        VotedPlaylistInterface votedPlaylist = new VotedPlaylist();
        TestInput testInput = new TestInput();
        votedPlaylist.addPlaylist(testInput.getPL1());
        votedPlaylist.addPlaylist(testInput.getPL2());
        votedPlaylist.addPlaylist(testInput.getPL3());

        List<TrackInterface> beforeSort = votedPlaylist.getVotedPlaylist().keySet().stream().toList();

        logger.log(Level.INFO, votedPlaylist::toString);

        List<TrackInterface> sortedPlaylist = new RandomSorter().sort(votedPlaylist);

        logger.log(Level.INFO, votedPlaylist::toString);
        StringBuilder sb = new StringBuilder();
        sb.append(String.format("Size equal: %b %n", beforeSort.size() == sortedPlaylist.size()));
        sb.append("Track is not a same place: \n");
        for (int i = 0; i < beforeSort.size(); i++) {
            sb.append(String.format("Track %d: %b %n", i + 1, beforeSort.get(i) == sortedPlaylist.get(i)));
        }
        logger.log(Level.INFO, sb::toString);

        new TerminalOutput().sendPlaylist(sortedPlaylist);
    }
}
