package com.opeter.automatadj.test;

import com.opeter.automatadj.model.TrackInterface;
import com.opeter.automatadj.model.VotedPlaylist;
import com.opeter.automatadj.model.VotedPlaylistInterface;
import com.opeter.automatadj.output.TerminalOutput;
import com.opeter.automatadj.sorter.BasicSorter;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public class StaticSorterTest {

    public void staticTest() {
        Logger logger = Logger.getLogger(StaticSorterTest.class.getName());

        VotedPlaylistInterface votedPlaylist = new VotedPlaylist();
        TestInput testInput = new TestInput();
        votedPlaylist.addPlaylist(testInput.getPL1());
        votedPlaylist.addPlaylist(testInput.getPL2());

        logger.log(Level.INFO, votedPlaylist::toString);

        List<TrackInterface> sortedPlaylist = new BasicSorter().sort(votedPlaylist);

        new TerminalOutput().sendPlaylist(sortedPlaylist);
    }
}
