package com.opeter.automatadj.test;

import com.opeter.automatadj.model.Playlist;
import com.opeter.automatadj.model.Track;

public class TestInput {

    public Playlist getPL1() {
        Playlist pl = new Playlist();
        pl.addTrack(new Track("Dire Straits", "Sultans of swing"));
        pl.addTrack(new Track("Dire Straits", "Sultans of swing"));
        pl.addTrack(new Track("Dire Straits", "Money for nothing"));
        pl.addTrack(new Track("Led Zeppelin", "Immigration song"));
        pl.addTrack(new Track("Bob Marley", "Buffalo soldiers"));
        return pl;
    }

    public Playlist getPL2() {
        Playlist pl = new Playlist();
        pl.addTrack(new Track("Dire Straits", "Sultans of swing"));
        pl.addTrack(new Track("Dire Straits", "Sultans of swing"));
        pl.addTrack(new Track("Dire Straits", "Money for nothing"));
        pl.addTrack(new Track("Led Zeppelin", "Immigration song"));
        pl.addTrack(new Track("Bob Marley", "Buffalo soldiers"));
        pl.addTrack(new Track("Bob Marley", "Buffalo soldiers"));
        return pl;
    }

    public Playlist getPL3() {
        Playlist pl = new Playlist();
        pl.addTrack(new Track("Led Zeppelin", "Black Dog"));
        pl.addTrack(new Track("Led Zeppelin", "Black Dog"));
        return pl;
    }
}
